defmodule H4id do
  @moduledoc """
  Documentation for H4id.
  """

  @doc """
  Generates a stream of valid ids from `start_num` to `end_num`

  ## Examples

      iex> H4id.generateIds(400_000, 400_001)
      #Stream<[enum: 400000..400001, funs: [#Function<47.85794499/1 in Stream.map/2>]]>
  """
  def generateIds(start_num, end_num) do
    start_num..end_num
    |> Stream.map( fn(id) -> appendCheckDigit(id) end )
  end

  defp appendCheckDigit id do
    sum =
    id
    |> Integer.digits
    |> Enum.zip(Stream.cycle([1, 3]))
    |> Enum.map(fn {x, y} -> x * y end)
    |> Enum.sum

    checkDig =
    0..9
    |> Stream.map(fn n -> if Integer.mod(sum + n, 10) == 0 do n end end)
    |> Enum.to_list
    |> Enum.filter(fn(item) -> item != nil end)

    id
    |> Integer.digits
    |> Enum.concat(checkDig)
    |> Enum.join
    |> appendLineBreak
  end

  defp appendLineBreak id do
    id
    |> String.pad_trailing(8, "\n")
  end


  @doc """
  Writes a file to ./output/output.txt with ids from `start_num` to `end_num`

  ## Examples

      iex> H4id.writeIdListToFile(400_000,400_100)
      :ok
  """
  def writeIdListToFile(start_num, end_num) do
    list = generateIds(start_num, end_num)
    |> Enum.to_list

    {:ok, file} = :file.open("./output/output.txt", [:write, :raw])
    :file.write(file, list)
  end

end


# [1, 2, 1, 2, 1] |> Enum.reverse |> Enum.with_index |> Enum.map(fn {num, index} when rem(index, 2) == 0 -> num+1; {num, _} -> num+3 end)

# list |> Enum.reverse |> Enum.zip(Stream.cycle([1, 3])) |> Enum.map(fn {x, y} -> x + y end) |> Enum.reverse
